﻿namespace FlowApi.Models;

public class FlowConfig
{
    public Guid Id { get; set; }
    public Flow Flow { get; set; }
    public Guid FlowId { get; set; }
    public StepCatalog StepCatalog { get; set; }  
    public Guid StepCatalogId { get; set; }
    public int Level {get; set; }
}