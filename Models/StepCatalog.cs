﻿namespace FlowApi.Models;

public class StepCatalog
{
    public Guid Id { get; set; }
    public string Code { get; set; }
    public string Name { get; set; }
    public string Target { get; set; }
    public string Description { get; set; }
}