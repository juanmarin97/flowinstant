﻿namespace FlowApi.Models;

public class Flow
{
    public Guid Id { get; set; }
    public string? Description { get; set; }  
}