﻿namespace FlowApi.Models;

public class FieldCatalog
{
    public Guid Id { get; set; }
    public string Code { get; set; }
    public string Name { get; set; }
}