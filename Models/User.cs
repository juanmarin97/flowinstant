﻿namespace FlowApi.Models;

public class User
{
    public Guid Id { get; set; }
    public FieldCatalog FieldCatalog { get; set; }
    public Guid FieldsCatalogId { get; set; }
    public string Value { get; set; }
}