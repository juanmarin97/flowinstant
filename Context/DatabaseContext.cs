﻿using FlowApi.Models;
using Microsoft.EntityFrameworkCore;

namespace FlowApi.Context;

public class DatabaseContext : DbContext
{
    public DbSet<StepCatalog> StepCatalogs { get; set; }
    public DbSet<FieldCatalog> FieldCatalogs { get; set; }
    public DbSet<User> Users { get; set; }
    public DbSet<Flow> Flows { get; set; }
    public DbSet<FlowConfig> FlowConfigs { get; set; }

    public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options){}
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // seed data, I know this is not the best way to do it, maybe move it to a different class
        #region DataSeed
        var stepsCatalogs = new List<StepCatalog>
        {
            new()
            {
                Id = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Code = "STP-0001",
                Name = "Paso 1",
                Target = "Step1",
                Description = ""
            },
            new()
            {
                Id = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa7"),
                Code = "STP-0002",
                Name = "Paso 1",
                Target = "Step2",
                Description = ""
            },
            new()
            {
                Id = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa8"),
                Code = "STP-0003",
                Name = "Paso 3",
                Target = "Step3",
                Description = ""
            },
            new()
            {
                Id = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa9"),
                Code = "STP-0004",
                Name = "Paso 4",
                Target = "Step4",
                Description = ""
            }
        };
        
        var fieldsCatalogs = new List<FieldCatalog>
        {
            new()
            {
                Id = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Code = "F-0001",
                Name = "Primer nombre"
            },
            new()
            {
                Id = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa7"),
                Code = "F-0002",
                Name = "Segundo nombre"
            }
        };

        var flows = new List<Flow>
        {
            new()
            {
                Id = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Description = "Flujo1"
            }
        };

        var flowConfigs = new List<FlowConfig>
        {
            new()
            {
                Id = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                FlowId = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                StepCatalogId = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Level = 1
            },
            new()
            {
                Id = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa7"),
                FlowId = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                StepCatalogId = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa7"),
                Level = 2
            },
            new()
            {
                Id = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa8"),
                FlowId = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                StepCatalogId = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa8"),
                Level = 3
            },
            new()
            {
                Id = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa9"),
                FlowId = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                StepCatalogId = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa9"),
                Level = 4
            }
        };

        var users = new List<User>
        {
            new()
            {
                Id = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                FieldsCatalogId =Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                Value = "Jhon"
            },
            new()
            {
                Id = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa7"),
                FieldsCatalogId = Guid.Parse("3fa85f64-5717-4562-b3fc-2c963f66afa7"),
                Value = "Doe"
            }
        };
        #endregion

        modelBuilder.Entity<StepCatalog>(stepCatalog =>
        {
            stepCatalog.ToTable("StepCatalog");
            stepCatalog.HasKey(p => p.Id);
            stepCatalog.Property(p => p.Code).HasMaxLength(50).IsRequired();
            stepCatalog.Property(p => p.Name).HasMaxLength(150).IsRequired();
            stepCatalog.Property(p => p.Target).HasMaxLength(150).IsRequired();
            stepCatalog.Property(p => p.Description).HasMaxLength(500);
            stepCatalog.HasData(stepsCatalogs);
        });
        
        modelBuilder.Entity<FieldCatalog>(fieldCatalog =>
        {
            fieldCatalog.ToTable("FieldCatalog");
            fieldCatalog.HasKey(p => p.Id);
            fieldCatalog.Property(p => p.Code).HasMaxLength(50).IsRequired();
            fieldCatalog.Property(p => p.Name).HasMaxLength(150).IsRequired();
            fieldCatalog.HasData(fieldsCatalogs);
        });
        
        modelBuilder.Entity<User>(user =>
        {
            user.ToTable("User");
            user.HasKey(p => p.Id);
            user.HasOne(x => x.FieldCatalog).WithMany().HasForeignKey(x => x.FieldsCatalogId);
            user.HasData(users);
        });
        
        modelBuilder.Entity<Flow>(flow =>
        {
            flow.ToTable("Flow");
            flow.HasKey(p => p.Id);
            flow.HasData(flows);
        });
        
        modelBuilder.Entity<FlowConfig>(flowConfig =>
        {
            flowConfig.ToTable("FlowConfig");
            flowConfig.HasKey(p => p.Id);
            flowConfig.Property(p => p.Level).IsRequired();
            flowConfig.HasOne(p => p.Flow).WithMany().HasForeignKey(x => x.FlowId);
            flowConfig.HasOne(p => p.StepCatalog).WithMany()
                .HasForeignKey(x => x.StepCatalogId);
            flowConfig.HasData(flowConfigs);
        });
    }
}