﻿using System.Reflection;
using FlowApi.Context;
using FlowApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FlowApi.Controllers;

[Route("api/[controller]")]
[ApiController]
public class FlowsController : Controller
{
   private readonly DatabaseContext _context;

        public FlowsController(DatabaseContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        // GET: api/Flows
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Flow>>> GetFlows()
        {
            if (_context.Flows == null)
            {
                return NotFound();
            }
            return await _context.Flows.ToListAsync();
        }

        // GET: api/Flows/5
        [HttpGet("GetFlowSteps/{id}")]
        public Task<ActionResult<IEnumerable<StepsViewModel>>> GetFlowSteps(Guid id)
        {
            if (_context.Flows == null)
            {
                return Task.FromResult<ActionResult<IEnumerable<StepsViewModel>>>(NotFound());
            }
            var flowSteps = _context.FlowConfigs
                .Where(x => x.FlowId.Equals(id))
                .Include(x => x.Flow)
                .Include(x => x.StepCatalog)
                .Select(x => new StepsViewModel
                {
                    StepName = x.StepCatalog.Name,
                    Target = x.StepCatalog.Target,
                    Level = x.Level
                })
                .OrderBy(x => x.Level)
                .ToList();

            if (flowSteps == null)
            {
                return Task.FromResult<ActionResult<IEnumerable<StepsViewModel>>>(NotFound());
            }

            return Task.FromResult<ActionResult<IEnumerable<StepsViewModel>>>(flowSteps);
        }

        // GET: api/Flows/5
        [HttpGet("StartFlow/{flowId}/{userId}")]
        public async Task<ActionResult<string>> StartFlow(Guid flowId, Guid userId)
        {
            var flowSteps = _context.FlowConfigs
                .Where(x => x.FlowId.Equals(flowId))
                .Include(x => x.Flow)
                .Include(x => x.StepCatalog)
                .Select(x => new StepsViewModel()
                {
                    StepName = x.StepCatalog.Name,
                    Target = x.StepCatalog.Target,
                    Level = x.Level
                })
                .OrderBy(x => x.Level)
                .ToList();

            var users = _context.Users
                .Where(x => x.Id.Equals(userId))
                .ToList();


            foreach (var step in flowSteps)
            {
                Console.WriteLine(step.StepName +" --- "+step.Level);
                await RunStepTarget(step.Target, users)!;
            }

            return Ok();
        }

        private Task<string>? RunStepTarget(string target, List<User> user)
        {
            try
            {
                var routeResult = GetStepTarget(target);
                return (Task<string>)routeResult.methodInfo.Invoke(routeResult.classInstance, new object[] { user})!;
            }
            catch
            {
                throw;
            }

        }

        private (MethodInfo? methodInfo, object? classInstance) GetStepTarget(string target)
        {
            Type uploaderDocumentsType = Type.GetType(target);
            object classInstance = Activator.CreateInstance(uploaderDocumentsType, null);
            MethodInfo? methodInfo = uploaderDocumentsType.GetMethod("RunStep");

            return (methodInfo, classInstance);
        }
}

public class StepsViewModel
{
    public string StepName { get; set; }
    public string Target { get; set; }
    public int Level { get; set; }
}