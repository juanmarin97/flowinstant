﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FlowApi.Context;
using FlowApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;


namespace FlowApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FieldsCatalogsController : Controller
    {
        private readonly DatabaseContext _context;

        public FieldsCatalogsController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/FieldsCatalogs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FieldCatalog>>> GetFieldsCatalogs()
        {
            if (_context.FieldCatalogs == null)
            {
                return NotFound();
            }

            return await _context.FieldCatalogs.ToListAsync();
        }

        // GET: api/FieldsCatalogs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FieldCatalog>> GetFieldsCatalog(Guid id)
        {
            if (_context.FieldCatalogs == null)
            {
                return NotFound();
            }

            var fieldsCatalog = await _context.FieldCatalogs.FindAsync(id);

            if (fieldsCatalog == null)
            {
                return NotFound();
            }

            return fieldsCatalog;
        }

        // PUT: api/FielsCatalogs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFieldsCatalog(Guid id, FieldCatalog fieldsCatalog)
        {
            if (id != fieldsCatalog.Id)
            {
                return BadRequest();
            }

            _context.Entry(fieldsCatalog).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FieldsCatalogExists(id))
                {
                    return NotFound();
                }

                throw;
            }

            return NoContent();
        }

        // POST: api/FieldsCatalogs
        [HttpPost]
        public async Task<ActionResult<FieldCatalog>> PostFieldsCatalog(FieldCatalog fieldsCatalog)
        {
            if (_context.FieldCatalogs == null)
            {
                return BadRequest("no data");
            }

            _context.FieldCatalogs.Add(fieldsCatalog);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFieldsCatalog", new { id = fieldsCatalog.Id }, fieldsCatalog);
        }

        // DELETE: api/FieldsCatalogs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFieldsCatalog(Guid id)
        {
            if (_context.FieldCatalogs == null)
            {
                return NotFound();
            }

            var FieldsCatalog = await _context.FieldCatalogs.FindAsync(id);
            if (FieldsCatalog == null)
            {
                return NotFound();
            }

            _context.FieldCatalogs.Remove(FieldsCatalog);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool FieldsCatalogExists(Guid id)
        {
            return (_context.FieldCatalogs?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}