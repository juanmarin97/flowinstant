﻿using FlowApi.Context;
using FlowApi.Models;

namespace FlowApi.Services;

public class UserService : IUserService
{
    private readonly DatabaseContext _context;

    public UserService(DatabaseContext context)
    {
        _context = context;
    }
    public IEnumerable<User> Get()
    {
        return _context.Users;
    }

    public async Task<User?> GetById(Guid id)
    {
        return await _context.Users.FindAsync(id);
    }

    public async Task Save(User user)
    {
        _context.Users.Add(user);
        await _context.SaveChangesAsync();
    }

    public Task Update(User user)
    {
        _context.Users.Update(user);
        _context.SaveChanges();
        return Task.CompletedTask;
    }
    
    public Task Delete(User user)
    {
        _context.Users.Remove(user);
        _context.SaveChanges();
        return Task.CompletedTask;
    }
}

public interface  IUserService
{
    IEnumerable<User> Get();
    Task<User?> GetById(Guid id);
    Task Save(User user);
    Task Update(User user);
    Task Delete(User user);
}