﻿# How to run this wonderful API

## Requirements
- .NET core 6 or higher 
- The database is in SQLite, so you don't need to install anything else
- No moree!!!!
 
## How to run
- dotnet run on command line
- The API will be running on http://localhost:7025
- You can use the swagger to test the API [swagger](http://localhost:7025/swagger/index.html)

[Have fun!](https://www.youtube.com/watch?v=KMU0tzLwhbE)